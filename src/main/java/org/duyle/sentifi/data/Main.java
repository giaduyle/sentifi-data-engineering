package org.duyle.sentifi.data;

import org.apache.commons.cli.*;
import org.duyle.sentifi.data.quandl.client.api.*;
import org.duyle.sentifi.data.quandl.client.calculation.*;
import org.duyle.sentifi.data.quandl.client.model.DataSet;
import org.duyle.sentifi.data.quandl.client.serializer.AlertSMADataSetSerializer;
import org.duyle.sentifi.data.quandl.client.serializer.CSVDataSetSerializer;
import org.duyle.sentifi.data.quandl.client.serializer.JSONDataSetSerializer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
public class Main {
    public static void main(String[] args) throws Exception {
        Logger logger = Logger.getLogger("Logger");
        String outputDir;
        String productCode;
        String dataSetCode;
        String apiKey;
        String order;

        //CLI Setup
        Options cliOpts = new Options();

        Option apiKeyOpt = new Option("a", "apikey", true, "Quandl API Key Code for querying data");
        apiKeyOpt.setRequired(false);
        cliOpts.addOption(apiKeyOpt);

        Option outputOpt = new Option("o", "output", true, "Output Directory Path for data files");
        outputOpt.setRequired(true);
        cliOpts.addOption(outputOpt);

        Option productCodeOpt = new Option("p", "product", true, "Quandl Product Code e.g. WIKI");
        productCodeOpt.setRequired(true);
        cliOpts.addOption(productCodeOpt);

        Option dataSetOpt = new Option("d", "dataset", true, "Quandl Dataset Code e.g. FB");
        dataSetOpt.setRequired(true);
        cliOpts.addOption(dataSetOpt);

        Option orderOpt = new Option("O", "order", true, "Quandl Order for ordering response and output data {\"asc\", \"desc\"}. Default is \"desc\"");
        orderOpt.setRequired(false);
        cliOpts.addOption(orderOpt);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        Client quandlClient;
        // Parse command line arguments
        try {
            cmd = parser.parse(cliOpts, args);
            apiKey = cmd.getOptionValue(apiKeyOpt.getLongOpt());
            outputDir = cmd.getOptionValue(outputOpt.getLongOpt());
            productCode = cmd.getOptionValue(productCodeOpt.getLongOpt());
            dataSetCode = cmd.getOptionValue(dataSetOpt.getLongOpt());
            order = cmd.getOptionValue(orderOpt.getLongOpt());

            //Set up output directory
            (new File(outputDir)).mkdirs();

            // Set up a HttpClient to connect to Quandl API Endpoint
            URLParams params = new URLParams();
            params.put("order", order != null ? order : "desc");
            if (apiKey != null) {
                params.put("api_key", apiKey);
            }

            quandlClient = new OkHttpClient(
                    new URLOptions.Builder()
                            .setApiVersion(3)
                            .setApi(APIType.TimeSeries)
                            .setProductCode(productCode)
                            .setDataSetCode(dataSetCode)
                            .setDataType(DataType.None)
                            .setFormat(DataFormat.JSON)
                            .build(),
                    params);


            // Fetch data from the Quandl API and calculate
            quandlClient.fetchData((InputStream input) -> {
                try {
                    FileHandler fileHandler = new FileHandler(outputDir + "/execute.log");
                    fileHandler.setFormatter(new SimpleFormatter());
                    logger.addHandler(fileHandler);

                    logger.info("Fetching data from server");
                    DataSet dataSet = (new GSONDataSetHandler()).handleResponse(input);

                    logger.info("Initializing calculation algorithms");
                    CalculationStrategy allStrategies = new CalculationStrategy()
                            .chainCalculationStrategy(new TWAPCalculation())
                            .chainCalculationStrategy(new SMACalculation(50))
                            .chainCalculationStrategy(new SMACalculation(200))
                            .chainCalculationStrategy(new LWMACalculation(15))
                            .chainCalculationStrategy(new LWMACalculation(50));

                    logger.info("Calculating all historical data set");
                    DataSet allCalculated = allStrategies.calculate(dataSet);

                    logger.info("Writing all historical data set");
                    File allJson = new File(outputDir + "/" + productCode + "/" + dataSetCode + "-All-Historical-Prices.json");
                    File allCSV = new File(outputDir + "/" + productCode + "/" + dataSetCode + "-All-Historical-Prices.csv");
                    (new JSONDataSetSerializer()).setSerializeOrder(dataSet.getOrder()).serializeToFile(allCalculated, allJson);
                    (new CSVDataSetSerializer()).setSerializeOrder(dataSet.getOrder()).serializeToFile(allCalculated, allCSV);

                    logger.info("Calculating alert points");
                    DataSet alertCalculated = (new AlertSMACalculation().calculate(allCalculated));

                    logger.info("Writing alert points");
                    File alertDat = new File(outputDir + "/" + productCode + "/" + dataSetCode + "-Alerts.dat");
                    (new AlertSMADataSetSerializer()).setSerializeOrder(dataSet.getOrder()).serializeToFile(alertCalculated, alertDat);

                } catch (IOException ex) {
                    logger.info("Error while executing: " + ex.getMessage());
                    ex.printStackTrace();
                    return false;
                } finally {
                    try {
                        input.close();
                        quandlClient.closeConnection();
                    } catch (IOException ex2) {
                        logger.info("Error while closing input stream: " + ex2.getMessage());
                        ex2.printStackTrace();
                    }
                }
                logger.info("All ran successfully!");
                return true;
            });
        } catch (ParseException parseEx) {
            logger.info("Error while parsing command line options: " + parseEx.getMessage());
            parseEx.printStackTrace();
            formatter.printHelp("help", cliOpts);
        }
    }
}
