package org.duyle.sentifi.data.quandl.client.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DataSet {
    public static final String ASC_ORDER = "asc";
    public static final String DESC_ORDER = "desc";
    private int id;
    private String dataset_code;
    private String database_code;
    private String name;
    private String description;
    private String refreshed_at;
    private String newest_available_date;
    private String oldest_available_date;
    private String[] column_names;
    private String frequency;
    private String type;
    private Boolean premium;
    private String limit;
    private String transform;
    private Integer column_index;
    private String start_date;
    private String end_date;
    private Object[] data;
    private Boolean collapse;
    private String order;
    private int database_id;

    private transient List<DataRecord> dataRecords;

    public DataSet() {
        dataRecords = new ArrayList<>();
    }

    public DataSet convertRawDataToModels() {
        for (int i = 0; i < data.length; i++) {
            DataRecord model = new DataRecord(this, i);
            List dataList = (List) data[i];
            for (int j = 0; j < column_names.length; j++) {
                model.setValue(column_names[j], dataList.get(j));
            }
            model.setValue("Ticker", getDatasetCode());
            dataRecords.add(model);
        }
        return this;
    }

    public DataRecord addDataRecord() {
        DataRecord newRecord = new DataRecord(this, dataRecords.size());
        dataRecords.add(newRecord);
        return newRecord;
    }

    public int getId() {
        return id;
    }

    public String getDatasetCode() {
        return dataset_code;
    }

    public String getDatabaseCode() {
        return database_code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getRefreshedAt() {
        return refreshed_at;
    }

    public String getNewestAvailableDate() {
        return newest_available_date;
    }

    public String getOldestAvailableDate() {
        return oldest_available_date;
    }

    public String getFrequency() {
        return frequency;
    }

    public String getType() {
        return type;
    }

    public Boolean getPremium() {
        return premium;
    }

    public String getLimit() {
        return limit;
    }

    public String getTransform() {
        return transform;
    }

    public Integer getColumnIndex() {
        return column_index;
    }

    public String getStartDate() {
        return start_date;
    }

    public String getEndDate() {
        return end_date;
    }

    public Boolean getCollapse() {
        return collapse;
    }

    public DataSet setDatasetCode(String code) {
        this.dataset_code = code;
        return this;
    }

    public DataSet setDatabaseCode(String code) {
        this.database_code = code;
        return this;
    }

    public DataSet setName(String name) {
        this.name = name;
        return this;
    }

    public DataSet setOrder(String order) {
        this.order = order;
         return this;
    }

    public void copyMetaData(DataSet dataSet) {
        this.dataset_code = dataSet.dataset_code;
        this.database_code = dataSet.database_code;
        this.name = dataSet.name;
        this.description = dataSet.description;
        this.refreshed_at = dataSet.refreshed_at;
        this.newest_available_date = dataSet.newest_available_date;
        this.oldest_available_date = dataSet.oldest_available_date;
        this.frequency = dataSet.frequency;
        this.type = dataSet.type;
        this.premium = dataSet.premium;
        this.limit = dataSet.limit;
        this.transform = dataSet.transform;
        this.column_index = dataSet.column_index;
        this.start_date = dataSet.start_date;
        this.end_date = dataSet.end_date;
        this.collapse = dataSet.collapse;
        this.order = dataSet.order;
        this.database_id = dataSet.database_id;
    }

    public String getOrder() {
        if (order == null) {
            return "desc";
        }
        return order;
    }

    public int getDatabaseId() {
        return database_id;
    }

    public String[] getColumnNames() {
        return column_names;
    }

    public Object[] getData() {
        return data;
    }

    public DataRecord[] getDataRecordsInRange(int start, int end) {
        DataRecord[] ret = new DataRecord[end - start];
        for (int i = start; i <= end && i < dataRecords.size(); i++) {
            ret[i] = dataRecords.get(i);
        }
        return ret;
    }

    public List<DataRecord> getAllDataRecords() {
        return dataRecords;
    }

    public DataRecord getOldestRecord() {
        return getOrder().equalsIgnoreCase(DESC_ORDER) ? dataRecords.get(dataRecords.size() - 1) : dataRecords.get(0);
    }

    public DataRecord getLatestRecord() {
        return getOrder().equalsIgnoreCase(DESC_ORDER) ? dataRecords.get(0) : dataRecords.get(dataRecords.size() - 1);
    }

    public Iterator<DataRecord> getLatestToOldestIterator() {
        return new LatestToOldestIterator();
    }

    public Iterator<DataRecord> getOldestToLatestIterator() {
        return new OldestToLatestIterator();
    }

    public Iterator<DataRecord> getIteratorForOrder(String order) {
        if (this.order.equals(order)) {
            return dataRecords.iterator();
        } else {
            if (order.equals(DESC_ORDER)) {
                return getLatestToOldestIterator();
            } else {
                return getOldestToLatestIterator();
            }
        }
    }

    private abstract class DataSetIterator implements Iterator<DataRecord> {
        int index = setUpIndex();
        int iterVector = setUpIteratingVector();

        @Override
        public boolean hasNext() {
            return index >= 0 && index < dataRecords.size();
        }

        @Override
        public DataRecord next() {
            DataRecord ret = dataRecords.get(index);
            index += iterVector;
            return ret;
        }

        abstract int setUpIndex();
        abstract int setUpIteratingVector();
    }

    private class OldestToLatestIterator extends DataSetIterator {
        @Override
        int setUpIndex() {
            return order.equalsIgnoreCase(ASC_ORDER) ? 0 : dataRecords.size() - 1;
        }

        @Override
        int setUpIteratingVector() {
            return order.equalsIgnoreCase(ASC_ORDER) ? +1 : -1;
        }
    }

    private class LatestToOldestIterator extends OldestToLatestIterator {
        @Override
        int setUpIndex() {
            return order.equalsIgnoreCase(ASC_ORDER) ? dataRecords.size() - 1 : 0;
        }

        @Override
        int setUpIteratingVector() {
            return order.equalsIgnoreCase(ASC_ORDER) ? -1 : +1;
        }
    }
}
