package org.duyle.sentifi.data.quandl.client.api;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class URLParams implements Serializable, Cloneable {
    private HashMap<String, Object> parameters;

    public URLParams() {
        parameters = new HashMap<>();
    }

    public URLParams put(String key, Object value) {
        if (value instanceof Boolean) {
            value = (Boolean) value ? 1 : 0;
        }

        parameters.put(key, value);
        return this;
    }

    public Object get(String key) {
        return parameters.get(key);
    }

    public Boolean getBoolean(String key) {
        Object value = get(key);
        if (value != null) {
            if ((value instanceof Integer) && ( (Integer) value == 1)) {
                return true;
            }
        }
        return false;
    }

    public URLParams remove(String key) {
        parameters.remove(key);
        return this;
    }

    public URLParams clear() {
        parameters.clear();
        return this;
    }

    public boolean containsKey(String key) {
        return parameters.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return parameters.containsValue(value);
    }

    public String buildURL() {
        try {
            StringBuilder retStr = new StringBuilder("?");
            for (Map.Entry<String, Object> m : parameters.entrySet()) {
                retStr.append(m.getKey()).append("=");
                retStr.append(URLEncoder.encode(String.valueOf(m.getValue()), "utf-8"));
                retStr.append("&");
            }
            return retStr.substring(0, retStr.length() - 1);
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public URLParams clone() {
        URLParams result = new URLParams();
        result.parameters = (HashMap<String, Object>) parameters.clone();
        return result;
    }
}
