package org.duyle.sentifi.data.quandl.client.calculation;

import org.duyle.sentifi.data.quandl.client.Utils;
import org.duyle.sentifi.data.quandl.client.model.DataRecord;
import org.duyle.sentifi.data.quandl.client.model.DataSet;

public class TWAPCalculation extends CalculationStrategy {
    private double cumulatedOpenPrice = 0.0f;
    private double cumulatedHighPrice = 0.0f;
    private double cumulatedLowPrice = 0.0f;
    private double cumulatedClosePrice = 0.0f;
    private double priceCount = 0.0f;

    public TWAPCalculation() {
        super();
    }

    @Override
    protected void calculateEachRecord(DataRecord input, DataRecord output) {
        super.calculateEachRecord(input, output);

        priceCount++;
        cumulatedOpenPrice += input.getDouble("Open");
        cumulatedHighPrice += input.getDouble("High");
        cumulatedLowPrice += input.getDouble("Low");
        cumulatedClosePrice += input.getDouble("Close");

        output.setValue("TWAP-Open", Utils.roundUpDouble(cumulatedOpenPrice / priceCount, 3));
        output.setValue("TWAP-High", Utils.roundUpDouble(cumulatedHighPrice / priceCount, 3));
        output.setValue("TWAP-Low", Utils.roundUpDouble(cumulatedLowPrice / priceCount, 3));
        output.setValue("TWAP-Close", Utils.roundUpDouble(cumulatedClosePrice / priceCount, 3));
    }
}
