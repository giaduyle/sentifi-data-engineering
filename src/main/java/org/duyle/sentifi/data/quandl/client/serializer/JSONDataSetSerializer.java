package org.duyle.sentifi.data.quandl.client.serializer;

import com.google.gson.*;
import org.duyle.sentifi.data.quandl.client.model.DataRecord;
import org.duyle.sentifi.data.quandl.client.model.DataSet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Iterator;

public class JSONDataSetSerializer extends DataSetSerializer<JsonElement> {
    private FileWriter fileWriter;

    public JSONDataSetSerializer(){
    }

    @Override
    public void serializeToFile(DataSet dataSet, File toFile) throws IOException {
        super.serializeToFile(dataSet, toFile);
        fileWriter = new FileWriter(toFile);
        serialize(dataSet);
        fileWriter.flush();
        fileWriter.close();
    }

    @Override
    public JsonElement serialize(DataSet result) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(DataSet.class, new JSONSerialize())
                .setPrettyPrinting()
                .create();
        JsonElement serialized = gson.toJsonTree(result);

        if (fileWriter != null) {
            gson.toJson(serialized, fileWriter);
        }

        return serialized;
    }

    private class JSONSerialize implements JsonSerializer<DataSet> {
        @Override
        public JsonElement serialize(DataSet dataSet, Type type, JsonSerializationContext context) {
            JsonArray prices = new JsonArray();

            Iterator<DataRecord> iter = getDataIterator(dataSet);
            while (iter.hasNext()) {
                DataRecord record = iter.next();
                JsonObject row = new JsonObject();

                row.add("Ticker", context.serialize(record.getString("Ticker")));
                row.add("Date", context.serialize(record.getString("Date")));
                row.add("Open", context.serialize(record.getDouble("Open")));
                row.add("High", context.serialize(record.getDouble("High")));
                row.add("Low", context.serialize(record.getDouble("Low")));
                row.add("Close", context.serialize(record.getDouble("Close")));
                row.add("Volume", context.serialize(record.getLong("Volume")));
                row.add("TWAP-Open", context.serialize(record.getDouble("TWAP-Open")));
                row.add("TWAP-High", context.serialize(record.getDouble("TWAP-High")));
                row.add("TWAP-Low", context.serialize(record.getDouble("TWAP-Low")));
                row.add("TWAP-Close", context.serialize(record.getDouble("TWAP-Close")));
                row.add("SMA-50", context.serialize(record.getDouble("SMA-50", 0.0f)));
                row.add("SMA-200", context.serialize(record.getDouble("SMA-200", 0.0f)));
                row.add("LWMA-15", context.serialize(record.getDouble("LWMA-15", 0.0f)));
                row.add("LWMA-50", context.serialize(record.getDouble("LWMA-50", 0.0f)));

                prices.add(row);
            }

            JsonObject ret = new JsonObject();
            ret.add("Prices", prices);
            return ret;
        }
    }
}
