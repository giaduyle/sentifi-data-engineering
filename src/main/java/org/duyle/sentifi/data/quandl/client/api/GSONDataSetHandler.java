package org.duyle.sentifi.data.quandl.client.api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.duyle.sentifi.data.quandl.client.model.DataSet;

import java.io.InputStream;
import java.io.InputStreamReader;

public class GSONDataSetHandler implements ResponseHandler<DataSet> {
    @Override
    public DataSet handleResponse(InputStream inputStream) {
        JsonParser parser = new JsonParser();
        JsonObject rootJSON = parser.parse(new InputStreamReader(inputStream)).getAsJsonObject();
        JsonElement datasetJSON = rootJSON.get("dataset");
        if (datasetJSON == null) {
            datasetJSON = rootJSON.get("dataset_data");
        }
        DataSet dataSet = (new Gson()).fromJson(datasetJSON, DataSet.class);
        dataSet.convertRawDataToModels();
        return dataSet;
    }
}
