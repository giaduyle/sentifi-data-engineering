package org.duyle.sentifi.data.quandl.client.calculation;

import org.duyle.sentifi.data.quandl.client.Utils;
import org.duyle.sentifi.data.quandl.client.model.DataRecord;
import org.duyle.sentifi.data.quandl.client.model.DataSet;

import java.util.LinkedList;

public class SMACalculation extends CalculationStrategy {
    private int smaPeriod;
    private final LinkedList<Double> smaPriceRange = new LinkedList<>();
    private double priceSum = 0.0f;

    public SMACalculation(int period) {
        super();
        smaPeriod = period;
    }

    @Override
    protected void calculateEachRecord(DataRecord input, DataRecord output) {
        super.calculateEachRecord(input, output);
        output.setValue("SMA-" + smaPeriod, (output.getIndex() + 1) >= smaPeriod ?
                Utils.roundUpDouble(calSMA(input.getDouble("Close")), 3) :
                0.0f);
    }

    private double calSMA(double price) {
        priceSum += price;
        smaPriceRange.add(price);
        if (smaPriceRange.size() > smaPeriod) {
            priceSum -= smaPriceRange.remove();
        }
        return priceSum / smaPriceRange.size();
    }
}
