package org.duyle.sentifi.data.quandl.client.calculation;

import org.duyle.sentifi.data.quandl.client.model.DataRecord;
import org.duyle.sentifi.data.quandl.client.model.DataSet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CalculationStrategy {
    private DataSet outputSet;
    private List<CalculationStrategy> chainedStrategies;

    public CalculationStrategy() {
    }

    public CalculationStrategy chainCalculationStrategy(CalculationStrategy calculationStrategy) {
        if (chainedStrategies == null) {
            chainedStrategies = new ArrayList<>();
        }
        chainedStrategies.add(calculationStrategy);
        return this;
    }

    public DataSet calculate(final DataSet dataSet) {
        outputSet = new DataSet();
        outputSet.copyMetaData(dataSet);

        Iterator<DataRecord> iter = dataSet.getOldestToLatestIterator();
        while (iter.hasNext()) {
            DataRecord input = iter.next();
            DataRecord output = outputSet.addDataRecord();
            calculateEachRecord(input, output);
        }

        return outputSet.setOrder("asc");
    }

    protected DataSet getOutputSet() {
        return outputSet;
    }

    protected void calculateEachRecord(final DataRecord input, final DataRecord output) {
        if (chainedStrategies != null && !chainedStrategies.isEmpty()) {
            for (CalculationStrategy pre : chainedStrategies) {
                pre.outputSet = output.getOwner();
                pre.calculateEachRecord(input, output);
            }
        }
        output.setValue("Ticker", output.getOwner().getDatasetCode());
        output.setValue("Date", input.getString("Date"));
        output.setValue("Volume", (long) input.getDouble("Volume").doubleValue());
        output.setValue("Open", input.getDouble("Open"));
        output.setValue("High", input.getDouble("High"));
        output.setValue("Low", input.getDouble("Low"));
        output.setValue("Close", input.getDouble("Close"));
    }
}
