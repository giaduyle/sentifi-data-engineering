package org.duyle.sentifi.data.quandl.client.calculation;

import org.duyle.sentifi.data.quandl.client.Utils;
import org.duyle.sentifi.data.quandl.client.model.DataRecord;
import org.duyle.sentifi.data.quandl.client.model.DataSet;

import java.util.Iterator;
import java.util.LinkedList;

public class LWMACalculation extends CalculationStrategy {
    private int lwmaPeriod;
    private final LinkedList<Double> lwmaPriceRange = new LinkedList<>();

    public LWMACalculation(int period) {
        super();
        lwmaPeriod = period;
    }

    @Override
    protected void calculateEachRecord(DataRecord input, DataRecord output) {
        super.calculateEachRecord(input, output);
        output.setValue("LWMA-" + lwmaPeriod, (output.getIndex() + 1) >= lwmaPeriod ?
                Utils.roundUpDouble(calLWMA(input.getDouble("Close")), 3) :
                0.0f);
    }

    private double calLWMA(double price) {
        lwmaPriceRange.add(price);
        if (lwmaPriceRange.size() > lwmaPeriod) {
            lwmaPriceRange.remove();
        }

        double sumPrice = 0.0f;

        int multiplier = lwmaPriceRange.size();
        int sumMultiplier = 0;

        Iterator<Double> backwardIter = lwmaPriceRange.descendingIterator();
        while (backwardIter.hasNext()) {
            sumPrice += backwardIter.next() * multiplier;
            sumMultiplier += multiplier;
            multiplier--;
        }
        return sumPrice / sumMultiplier;
    }
}
