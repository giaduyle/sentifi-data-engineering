package org.duyle.sentifi.data.quandl.client.api;

import java.io.InputStream;

public interface ResponseHandler<OUTPUT> {
    OUTPUT handleResponse(InputStream input);
}
