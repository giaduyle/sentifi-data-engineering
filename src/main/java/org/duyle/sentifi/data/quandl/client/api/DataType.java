package org.duyle.sentifi.data.quandl.client.api;

public enum DataType {
    None(null),
    DataOnly("data"),
    MetaDataOnly("metadata");

    private String value;

    DataType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
