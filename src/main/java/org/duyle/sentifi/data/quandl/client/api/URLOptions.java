package org.duyle.sentifi.data.quandl.client.api;

import java.io.Serializable;

import static org.duyle.sentifi.data.quandl.client.api.DataFormat.JSON;

public class URLOptions implements Serializable {
    private String host;
    private boolean ssl;
    private int apiVersion;
    private APIType api;
    private String productCode;
    private String dataSetCode;
    private DataType dataType;
    private DataFormat format;

    public URLOptions(URLOptions.Builder builder) {
        this.host = builder.host;
        this.ssl = builder.ssl;
        this.apiVersion = builder.apiVersion;
        this.api = builder.api;
        this.productCode = builder.productCode;
        this.dataSetCode = builder.dataSetCode;
        this.dataType = builder.dataType;
        this.format = builder.format;
    }

    public String getHost(){
        return host;
    }

    public boolean isSecured() {
        return ssl;
    }

    public int getApiVersion() {
        return apiVersion;
    }

    public APIType getApi() {
        return api;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getDataSetCode() {
        return dataSetCode;
    }

    public DataType getDataType() {
        return dataType;
    }

    public DataFormat getFormat() {
        return format;
    }

    public String buildURL() {
        StringBuilder APIUrl = new StringBuilder()
                .append(ssl ? "https://" : "http://").append(host)
                .append("/").append("v").append(String.valueOf(apiVersion))
                .append("/").append(api.toString())
                .append("/").append(productCode.toUpperCase())
                .append("/").append(dataSetCode.toUpperCase());
        String dataTypeValue = dataType.toString();
        if (dataTypeValue != null && !dataTypeValue.isEmpty()) {
            APIUrl.append("/").append(dataTypeValue);
        }
        APIUrl.append(".").append(format.name().toLowerCase());
        return APIUrl.toString();
    }

    public URLOptions.Builder newBuilder() {
        return new URLOptions.Builder(this);
    }

    public static class Builder {
        private String host = "www.quandl.com/api";
        private boolean ssl = true;
        private int apiVersion = 1;
        private APIType api;
        private String productCode;
        private String dataSetCode;
        private DataType dataType;
        private DataFormat format = JSON;

        public Builder() {
        }

        public Builder(URLOptions ops) {
            this.host = ops.host;
            this.ssl = ops.ssl;
            this.apiVersion = ops.apiVersion;
            this.api = ops.api;
            this.productCode = ops.productCode;
            this.dataSetCode = ops.dataSetCode;
            this.dataType = ops.dataType;
            this.format = ops.format;
        }

        public Builder setHost(String host) {
            this.host = host;
            return this;
        }

        public Builder setSsl(boolean ssl) {
            this.ssl = ssl;
            return this;
        }

        public Builder setApiVersion(int apiVersion) {
            this.apiVersion = apiVersion;
            return this;
        }

        public Builder setApi(APIType api) {
            this.api = api;
            return this;
        }

        public Builder setFormat(DataFormat format) {
            this.format = format;
            return this;
        }

        public Builder setProductCode(String productCode) {
            this.productCode = productCode;
            return this;
        }

        public Builder setDataSetCode(String dataSetCode) {
            this.dataSetCode = dataSetCode;
            return this;
        }

        public Builder setDataType(DataType dataType) {
            this.dataType = dataType;
            return this;
        }

        public URLOptions build() {
            return new URLOptions(this);
        }
    }
}
