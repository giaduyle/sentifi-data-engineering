package org.duyle.sentifi.data.quandl.client.api;

public enum APIType {
    Databases("databases"),
    TimeSeries("datasets"),
    Tables("datatables");

    private String value;

    APIType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
