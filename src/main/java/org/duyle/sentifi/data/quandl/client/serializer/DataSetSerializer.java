package org.duyle.sentifi.data.quandl.client.serializer;

import org.duyle.sentifi.data.quandl.client.model.DataRecord;
import org.duyle.sentifi.data.quandl.client.model.DataSet;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

public abstract class DataSetSerializer<OUTPUT> {
    private String serializeOrder = DataSet.DESC_ORDER;

    public DataSetSerializer() {
    }

    public DataSetSerializer setSerializeOrder(String order) {
        this.serializeOrder = order;
        return this;
    }

    public Iterator<DataRecord> getDataIterator(DataSet dataSet) {
        return dataSet.getIteratorForOrder(serializeOrder);
    }

    public abstract OUTPUT serialize(DataSet dataSet);

    public void serializeToFile(DataSet dataSet, File toFile) throws IOException {
        toFile.getParentFile().mkdir();
    }
}
