package org.duyle.sentifi.data.quandl.client.serializer;

import org.duyle.sentifi.data.quandl.client.model.DataRecord;
import org.duyle.sentifi.data.quandl.client.model.DataSet;

import java.util.Iterator;

public class AlertSMADataSetSerializer extends CSVDataSetSerializer {
    public AlertSMADataSetSerializer(){
    }

    @Override
    public String serialize(DataSet dataSet) {
        StringBuilder retStr = new StringBuilder();

        // Header row
        retStr
                .append("Alert").append(SEPERATOR)
                .append("Ticker").append(SEPERATOR)
                .append("Date").append(SEPERATOR)
                .append("Open").append(SEPERATOR)
                .append("High").append(SEPERATOR)
                .append("Low").append(SEPERATOR)
                .append("Close").append(SEPERATOR)
                .append("Volume").append(SEPERATOR)
                .append("VolumeAverage-50").append(SEPERATOR)
                .append("SMA-50").append(SEPERATOR)
                .append("SMA-200");

        Iterator<DataRecord> iter = getDataIterator(dataSet);
        while (iter.hasNext()) {
            DataRecord record = iter.next();
            retStr
                    .append(System.lineSeparator())
                    .append(record.getString("Alert")).append(SEPERATOR)
                    .append(record.getString("Ticker")).append(SEPERATOR)
                    .append(record.getString("Date")).append(SEPERATOR)
                    .append(record.getDouble("Open")).append(SEPERATOR)
                    .append(record.getDouble("High")).append(SEPERATOR)
                    .append(record.getDouble("Low")).append(SEPERATOR)
                    .append(record.getDouble("Close")).append(SEPERATOR)
                    .append(record.getLong("Volume")).append(SEPERATOR)
                    .append(record.getString("VolumeAverage-50", "")).append(SEPERATOR)
                    .append(record.getDouble("SMA-50", 0.0f)).append(SEPERATOR)
                    .append(record.getDouble("SMA-200", 0.0f));
        }

        return retStr.toString();
    }
}
