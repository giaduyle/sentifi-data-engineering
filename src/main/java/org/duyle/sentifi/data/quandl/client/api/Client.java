package org.duyle.sentifi.data.quandl.client.api;

import java.io.IOException;
import java.io.Serializable;

public abstract class Client implements Serializable, Cloneable {
    private URLOptions options;
    private URLParams params;
    private int lastResponseCode = -1;
    private int connectTimeout = 15*1000;
    private int readTimeout = 1*60*1000;

    public Client(URLOptions opts, URLParams params) {
        this.options = opts;
        this.params = params;
    }

    public URLOptions getOptions() {
        return options;
    }

    public void setOptions(URLOptions options) {
        this.options = options;
    }

    public URLParams getParams() {
        return params;
    }

    public void setParams(URLParams params) {
        this.params = params;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    protected void setLastResponseCode(int code) {
        lastResponseCode = code;
    }

    public int getLastResponseCode() {
        return lastResponseCode;
    }

    public abstract void fetchData(ResponseHandler handler) throws IOException;

    public abstract void closeConnection();
}
