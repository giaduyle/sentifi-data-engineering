package org.duyle.sentifi.data.quandl.client.api;

public enum DataFormat {
    JSON,
    XML,
    CSV;
}
