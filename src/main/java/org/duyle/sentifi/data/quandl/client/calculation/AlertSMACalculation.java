package org.duyle.sentifi.data.quandl.client.calculation;

import org.duyle.sentifi.data.quandl.client.model.DataRecord;
import org.duyle.sentifi.data.quandl.client.model.DataSet;

import java.util.Iterator;
import java.util.LinkedList;

public class AlertSMACalculation extends CalculationStrategy {
    private final LinkedList<Double> volumeRange = new LinkedList<>();
    private double volumeSum = 0.0f;

    public AlertSMACalculation() {
    }

    @Override
    public DataSet calculate(DataSet dataSet) {
        DataSet outputSet = new DataSet();
        outputSet.copyMetaData(dataSet);

        Iterator<DataRecord> iter = dataSet.getOldestToLatestIterator();

        while (iter.hasNext()) {
            DataRecord input = iter.next();

            double sma50 = input.getDouble("SMA-50", -1f);
            double sma200 = input.getDouble("SMA-200", -1f);
            if (sma50 > 0.0f && sma200 > 0.0f) {
                if (sma50 < sma200) {
                    DataRecord output = outputSet.addDataRecord();
                    calculateEachRecord(input, output);
                    output.setValue("Alert", "bearish");
                    output.setValue("SMA-50", sma50);
                    output.setValue("SMA-200", sma200);
                } else if ((sma50 > sma200) && trackVolume(input)) {
                    DataRecord output = outputSet.addDataRecord();
                    calculateEachRecord(input, output);
                    output.setValue("Alert", "bullish");
                    output.setValue("VolumeAverage-50", (long) (volumeSum / 50));
                    output.setValue("SMA-50", sma50);
                    output.setValue("SMA-200", sma200);
                }
            }
        }

        return outputSet;
    }

    private boolean trackVolume(DataRecord input) {
        double inVolume = input.getDouble("Volume");
        volumeSum += inVolume;
        volumeRange.add(inVolume);
        if (volumeRange.size() > 50) {
            volumeSum -= volumeRange.remove();
        }
        return (inVolume + inVolume * 10 / 100) > (volumeSum / 50);
    }
}
