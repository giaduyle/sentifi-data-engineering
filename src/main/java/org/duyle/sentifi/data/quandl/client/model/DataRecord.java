package org.duyle.sentifi.data.quandl.client.model;

import java.util.HashMap;
import java.util.Map;

public class DataRecord {
    private int index;
    private DataSet owner;
    private Map<String, Object> values;

    public DataRecord() {
        values = new HashMap<>();
    }

    public DataRecord(DataSet owner, int index) {
        this();
        this.owner = owner;
        this.index = index;
    }

    public DataSet getOwner() {
        return owner;
    }

    public DataRecord setValue(String name, Object val) {
        values.put(name, val);
        return this;
    }

    public Object getValue(String name) {
        return values.get(name);
    }

    public String getString(String name) {
        return String.valueOf(getValue(name));
    }

    public String getString(String name, String defaultVal) {
        Object val = getValue(name);
        if (val != null) {
            return String.valueOf(getValue(name));
        }
        return defaultVal;
    }

    public Integer getInteger(String name) {
        try {
            Number val = getFloat(name);
            return val != null ? val.intValue() : null;
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public Integer getInteger(String name, int defaultValue) {
        Integer ret = getInteger(name);
        return (ret != null) ? ret : defaultValue;
    }

    public Long getLong(String name) {
        try {
            Number val = getDouble(name);
            return val != null ? val.longValue() : null;
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public Long getLong(String name, long defaultValue) {
        Long ret = getLong(name);
        return (ret != null) ? ret : defaultValue;
    }

    public Float getFloat(String name) {
        try {
            return Float.valueOf(getString(name));
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public Float getFloat(String name, float defaultValue) {
        Float ret = getFloat(name);
        return (ret != null) ? ret : defaultValue;
    }

    public Double getDouble(String name) {
        try {
            return Double.valueOf(getString(name));
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public Double getDouble(String name, double defaultValue) {
        Double ret = getDouble(name);
        return (ret != null) ? ret : defaultValue;
    }

    public Boolean getBoolean(String name) {
        try {
            return Boolean.valueOf(getString(name));
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public int getIndex() {
        return index;
    }
}
