package org.duyle.sentifi.data.quandl.client;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

public class Utils {
    public static String encodeBase64(String input) {
        try {
            return new String(Base64.getEncoder().encode(input.getBytes("utf-8")));
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String decodeBase64(String base64String) {
        return new String(Base64.getDecoder().decode(base64String));
    }

    public static String getStringFromInputStream(InputStream is) throws IOException {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    public static double roundUpDouble(double val, int scale) {
        return (new BigDecimal(val)).setScale(scale, RoundingMode.HALF_UP).doubleValue();
    }

    public static Path getResourceDir() {
        return Paths.get("src", "main", "resources");
    }
}
