package org.duyle.sentifi.data.quandl.client.api;

import okhttp3.CacheControl;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class OkHttpClient extends Client {
    private static final MediaType PLAINTEXT_MEDIA_TYPE = MediaType.parse("text/plaintext; charset=utf-8");

    private okhttp3.OkHttpClient httpClient = new okhttp3.OkHttpClient();
    private boolean useCache = false;

    public OkHttpClient(URLOptions ops, URLParams params) {
        super(ops, params);
    }

    public boolean isUseCache() {
        return useCache;
    }

    public void setUseCache(boolean useCache) {
        this.useCache = useCache;
    }

    @Override
    public void fetchData(ResponseHandler handler) throws IOException {
        String optsURL = getOptions().buildURL();
        String paramsURL = getParams().buildURL();

        httpClient = httpClient.newBuilder()
                .connectTimeout(getConnectTimeout(), TimeUnit.MILLISECONDS)
                .readTimeout(getReadTimeout(), TimeUnit.MILLISECONDS).build();

        Request request = new Request.Builder()
                .url(optsURL + paramsURL)
                .cacheControl(useCache ? CacheControl.FORCE_CACHE : CacheControl.FORCE_NETWORK)
                .method("GET", null)
                .build();

        Response response = httpClient.newCall(request).execute();

        setLastResponseCode(response.code());

        handler.handleResponse(response.body().byteStream());
    }

    @Override
    public void closeConnection() {
        httpClient.connectionPool().evictAll();
    }
}
