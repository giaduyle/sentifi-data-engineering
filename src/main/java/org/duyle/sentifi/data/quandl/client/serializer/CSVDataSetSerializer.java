package org.duyle.sentifi.data.quandl.client.serializer;

import org.duyle.sentifi.data.quandl.client.model.DataRecord;
import org.duyle.sentifi.data.quandl.client.model.DataSet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

public class CSVDataSetSerializer extends DataSetSerializer<String> {
    protected static final String SEPERATOR = ",";

    public CSVDataSetSerializer(){
    }

    @Override
    public void serializeToFile(DataSet dataSet, File toFile) throws IOException {
        super.serializeToFile(dataSet, toFile);
        FileWriter fileWriter = new FileWriter(toFile);
        fileWriter.write(serialize(dataSet));
        fileWriter.flush();
        fileWriter.close();
    }

    @Override
    public String serialize(DataSet dataSet) {
        StringBuilder retStr = new StringBuilder();

        // Header row
        retStr
                .append("Ticker").append(SEPERATOR)
                .append("Date").append(SEPERATOR)
                .append("Open").append(SEPERATOR)
                .append("High").append(SEPERATOR)
                .append("Low").append(SEPERATOR)
                .append("Close").append(SEPERATOR)
                .append("Volume").append(SEPERATOR)

                .append("TWAP-Open").append(SEPERATOR)
                .append("TWAP-High").append(SEPERATOR)
                .append("TWAP-Low").append(SEPERATOR)
                .append("TWAP-Close").append(SEPERATOR)

                .append("SMA-50").append(SEPERATOR)
                .append("SMA-200").append(SEPERATOR)
                .append("LWMA-15").append(SEPERATOR)
                .append("LWMA-50");

        Iterator<DataRecord> iter = getDataIterator(dataSet);
        while (iter.hasNext()) {
            DataRecord record = iter.next();
            retStr
                    .append(System.lineSeparator())
                    .append(record.getString("Ticker")).append(SEPERATOR)
                    .append(record.getString("Date")).append(SEPERATOR)
                    .append(record.getDouble("Open")).append(SEPERATOR)
                    .append(record.getDouble("High")).append(SEPERATOR)
                    .append(record.getDouble("Low")).append(SEPERATOR)
                    .append(record.getDouble("Close")).append(SEPERATOR)
                    .append(record.getLong("Volume")).append(SEPERATOR)

                    .append(record.getDouble("TWAP-Open", 0.0f)).append(SEPERATOR)
                    .append(record.getDouble("TWAP-High", 0.0f)).append(SEPERATOR)
                    .append(record.getDouble("TWAP-Low", 0.0f)).append(SEPERATOR)
                    .append(record.getDouble("TWAP-Close", 0.0f)).append(SEPERATOR)
                    .append(record.getDouble("SMA-50", 0.0f)).append(SEPERATOR)
                    .append(record.getDouble("SMA-200", 0.0f)).append(SEPERATOR)
                    .append(record.getDouble("LWMA-15", 0.0f)).append(SEPERATOR)
                    .append(record.getDouble("LWMA-50", 0.0f));
        }

        return retStr.toString();
    }
}
