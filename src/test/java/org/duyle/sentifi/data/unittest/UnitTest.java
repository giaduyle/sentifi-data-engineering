package org.duyle.sentifi.data.unittest;

import org.duyle.sentifi.data.quandl.client.api.*;
import org.duyle.sentifi.data.quandl.client.calculation.*;
import org.duyle.sentifi.data.quandl.client.model.DataRecord;
import org.duyle.sentifi.data.quandl.client.model.DataSet;
import org.duyle.sentifi.data.quandl.client.serializer.AlertSMADataSetSerializer;
import org.duyle.sentifi.data.quandl.client.serializer.CSVDataSetSerializer;
import org.duyle.sentifi.data.quandl.client.serializer.JSONDataSetSerializer;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

public class UnitTest {
    private Client quandlClient;
    private DataSet fetchDataSet;
    private File allPricesJSON;
    private File allPricesCSV;
    private File alertDat;
    private File targetSentifiDir;


    @BeforeClass
    public void setUp() {
        targetSentifiDir = new File(Paths.get("target", "Sentifi_Outputs").toUri());
        targetSentifiDir.mkdirs();
        quandlClient = new OkHttpClient(
                new URLOptions.Builder()
                        .setApiVersion(3)
                        .setApi(APIType.TimeSeries)
                        .setProductCode("WIKI")
                        .setDataSetCode("FB")
                        .setDataType(DataType.None)
                        .setFormat(DataFormat.JSON)
                        .build(),
                new URLParams()
                        .put("order", "asc")
                        .put("api_key", "_yZ_bwUxKSYyB2VmgJu2"));
        allPricesJSON = new File(targetSentifiDir.getPath() + "/allPrices.json");
        allPricesCSV = new File(targetSentifiDir.getPath() + "/allPrices.csv");
        alertDat = new File(targetSentifiDir.getPath() + "/alerts.dat");
    }

    @AfterClass
    public void tearDown() {
        quandlClient.closeConnection();
    }

    @Test(priority = 1)
    public void testFetchData() throws IOException {
        quandlClient.fetchData((InputStream input) -> {
            fetchDataSet = (new GSONDataSetHandler()).handleResponse(input);
            try {
                input.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return true;
        });

        Assert.assertEquals(quandlClient.getLastResponseCode(), 200);
        Assert.assertEquals(fetchDataSet.getDatabaseCode(), "WIKI");
        Assert.assertEquals(fetchDataSet.getDatasetCode(), "FB");
        Assert.assertTrue(!fetchDataSet.getAllDataRecords().isEmpty());
        Assert.assertTrue(fetchDataSet.getAllDataRecords().size() >= 1451);
    }

    @Test(priority =  2)
    public void testTWAPCalculation() {
        DataSet twapSet = (new TWAPCalculation()).calculate(fetchDataSet);
        Assert.assertEquals(twapSet.getAllDataRecords().size(), fetchDataSet.getAllDataRecords().size());

        testDataSetBasic(twapSet, fetchDataSet);

        // Test oldest record
        DataRecord oldestTWAP = twapSet.getOldestRecord();
        Assert.assertEquals(oldestTWAP.getDouble("TWAP-Open"), 42.05);
        Assert.assertEquals(oldestTWAP.getDouble("TWAP-High"), 45.0);
        Assert.assertEquals(oldestTWAP.getDouble("TWAP-Low"), 38.0);
        Assert.assertEquals(oldestTWAP.getDouble("TWAP-Close"), 38.232);

        // Test latest record
        DataRecord latestTWAP = twapSet.getLatestRecord();
        Assert.assertEquals(latestTWAP.getDouble("TWAP-Open"), 88.297);
        Assert.assertEquals(latestTWAP.getDouble("TWAP-High"), 89.193);
        Assert.assertEquals(latestTWAP.getDouble("TWAP-Low"), 87.335);
        Assert.assertEquals(latestTWAP.getDouble("TWAP-Close"), 88.299);
    }

    @DataProvider(name = "SMA-Periods")
    public Object[][] SMAPeriods() {
        return new Object[][] {
                {50, 23.15},
                {200, 28.578}
        };
    }
    @Test(priority = 3, dataProvider = "SMA-Periods")
    public void testSMACalculation(int period, double expectedSMA) {
        DataSet smaSet = (new SMACalculation(period)).calculate(fetchDataSet);

        testDataSetBasic(smaSet, fetchDataSet);

        // Test the first Nth - 1 SMA records
        for (int i = 0; i < period - 2; i++) {
            Assert.assertEquals(smaSet.getAllDataRecords().get(i).getDouble("SMA-" + period), Double.valueOf(0));
        }

        // Test Nth SMA record
        DataRecord nthRecord = smaSet.getAllDataRecords().get(period - 1);
        Assert.assertEquals(nthRecord.getDouble("SMA-" + period), expectedSMA);
    }

    @DataProvider(name = "LWMA-Periods")
    public Object[][] LWMAPeriods() {
        return new Object[][] {
                {15, 27.1},
                {50, 23.15}
        };
    }
    @Test(priority = 4, dataProvider = "LWMA-Periods")
    public void testLWMACalculation(int period, double expectedLWMA) {
        DataSet lwmaSet = (new LWMACalculation(period)).calculate(fetchDataSet);

        testDataSetBasic(lwmaSet, fetchDataSet);

        // Test the first Nth - 1 LWMA records
        for (int i = 0; i < period - 2; i++) {
            Assert.assertEquals(lwmaSet.getAllDataRecords().get(i).getDouble("LWMA-" + period), Double.valueOf(0));
        }

        // Test Nth LWMA record
        DataRecord nthRecord = lwmaSet.getAllDataRecords().get(period - 1);
        Assert.assertEquals(nthRecord.getDouble("LWMA-" + period), expectedLWMA);
    }

    @Test(priority = 5)
    public void testAlertCalculation() throws IOException {
        DataSet sma50200Set = (new SMACalculation(50).chainCalculationStrategy(new SMACalculation(200))).calculate(fetchDataSet);
        DataSet alertSet = (new AlertSMACalculation()).calculate(sma50200Set);

        for (DataRecord record : alertSet.getAllDataRecords()) {
            String alert = record.getString("Alert");
            Assert.assertTrue(!alert.isEmpty());
            Assert.assertTrue(!record.getString("Ticker").isEmpty());
            Assert.assertTrue(!record.getString("Date").isEmpty());
            Assert.assertTrue(record.getDouble("Open") != null);
            Assert.assertTrue(record.getDouble("High") != null);
            Assert.assertTrue(record.getDouble("Low") != null);
            Assert.assertTrue(record.getDouble("Close") != null);
            Assert.assertTrue(record.getDouble("Volume") != null);
            Assert.assertTrue(record.getDouble("SMA-50") != null);
            Assert.assertTrue(record.getDouble("SMA-200") != null);

            Double volumenAvg50 = record.getDouble("VolumeAverage-50");
            if (alert.equals("bearish")) {
                Assert.assertTrue(volumenAvg50 == null);
            } else if (alert.equals("bullish")) {
                Assert.assertTrue(volumenAvg50 != null);
                Assert.assertTrue(volumenAvg50 > 0.0f);
            }
        }

        (new AlertSMADataSetSerializer()).setSerializeOrder("asc").serializeToFile(alertSet, alertDat);
    }

    @Test(priority = 6)
    public void testFileOutput() throws IOException {
        CalculationStrategy allStrategies = new CalculationStrategy()
                .chainCalculationStrategy(new TWAPCalculation())
                .chainCalculationStrategy(new SMACalculation(50))
                .chainCalculationStrategy(new SMACalculation(200))
                .chainCalculationStrategy(new LWMACalculation(15))
                .chainCalculationStrategy(new LWMACalculation(50));

        DataSet allPricesSet = allStrategies.calculate(fetchDataSet);
        (new JSONDataSetSerializer()).setSerializeOrder("asc").serializeToFile(allPricesSet, allPricesJSON);
        (new CSVDataSetSerializer()).setSerializeOrder("asc").serializeToFile(allPricesSet, allPricesCSV);

        Assert.assertTrue(allPricesJSON.exists());
        Assert.assertTrue(allPricesCSV.exists());
        Assert.assertTrue(alertDat.exists());
    }

    private void testDataSetBasic(DataSet sourceSet, DataSet targetSet) {
        testDataRecordBasic(sourceSet.getOldestRecord(), targetSet.getOldestRecord());
        testDataRecordBasic(sourceSet.getLatestRecord(), targetSet.getLatestRecord());
    }

    private void testDataRecordBasic(DataRecord source, DataRecord target) {
        Assert.assertEquals(source.getString("Ticker"), target.getString("Ticker"));
        Assert.assertEquals(source.getString("Date"), target.getString("Date"));
        Assert.assertEquals(source.getDouble("Open"), target.getDouble("Open"));
        Assert.assertEquals(source.getDouble("High"), target.getDouble("High"));
        Assert.assertEquals(source.getDouble("Low"), target.getDouble("Low"));
        Assert.assertEquals(source.getDouble("Close"), target.getDouble("Close"));
        Assert.assertEquals(source.getLong("Volume"), target.getLong("Volume"));
    }
}
