* _Run Instructions_:
    * A pre-built jar file named **data.jar** is located at _projectRootDir/binaries_
    * Navigate to it and simply run:
        * Run command: `$ java -jar data.jar --output "/Volumes/Data/Sentifi_Outputs" --product "WIKI" --dataset "FB" --order "asc"`
        * Parameters (to print out parameters usage, type in `--help` switch in the command line):
            * `o, --output`: Output Directory Path for the data files (Required)
            * `a, --apiKey`: Quandl API Key Code for querying data (Optional)
            * `p, --product`: Quandl Product Code e.g. WIKI (Required)
            * `d, --dataset`: Quandl Dataset Code e.g. FB (Required)
            * `O, --order`: Quandl Order for ordering response and output data {"asc", "desc"}. Default is "desc" (Optional)
        
* _Build From Source Instructions_:
    * This project uses Maven as the build tool
    * Navigate the _projectRootDir_ and run following commands:
        * `$ mvn install` -> to install and resolve dependencies
        * `$ mvn package` -> to compile the sources and build the following jar files at this folder _projectRootDir/target_
            * **data-1.0.jar**: this jar is a minimalist jar without the dependencies packaged, so it is ideal for embedding into bigger projects
            * **data-1.0-jar-with-dependencies.jar**: this is the full jar file with all dependencies included, it is idea for instant running from the command line (see _Run Instructions_ section)
